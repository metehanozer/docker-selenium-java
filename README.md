# 🎁 docker-selenium-java
Paralel web UI test with Docker Selenium Grid

### Run test in local
~~~
./mvnw clean test -Dsuite=testng-local -Dbrowser=local-chrome
./mvnw clean test -Dsuite=testng-local -DthreadCount=1 -Dbrowser=local-firefox
~~~

### Run test in docker with chrome or firefox
~~~
docker-compose up -d
./mvnw clean test -Dsuite=testng-docker -Dbrowser=docker-chrome
./mvnw clean test -Dsuite=testng-docker -Dbrowser=docker-firefox
docker-compose down
~~~

### Run test in docker with chrome & firefox
~~~
docker-compose up -d
./mvnw clean test -Dsuite=testng-docker-all
docker-compose down
~~~