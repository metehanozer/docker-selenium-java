package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utils.PageUtil;

public class HomePage extends PageUtil {

    private final By searchInput = By.xpath("//input[@id='suggestion-search']");
    private final By searchButton = By.xpath("//button[@id='suggestion-search-button']");

    public HomePage(WebDriver driver){
        super(driver);
    }

    public SearchResultPage searchKeyword(String keyword) {
        sendKeysTo(searchInput, keyword);
        clickTo(searchButton);
        return new SearchResultPage(webDriver);
    }
}
