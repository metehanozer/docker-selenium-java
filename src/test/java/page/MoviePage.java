package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utils.PageUtil;

public class MoviePage extends PageUtil {

    private final By releaseDate = By.cssSelector("a[title='See more release dates']");

    public MoviePage(WebDriver webDriver){
        super(webDriver);
    }

    public String getReleaseDate() {
        return getTextTo(releaseDate);
    }
}
