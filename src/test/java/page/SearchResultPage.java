package page;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.PageUtil;

public class SearchResultPage extends PageUtil {

    private final By movieFilter = By.linkText("Movie");
    private final By searchResultTable = By.xpath("//tbody/tr/td[2]/a[1]");

    public SearchResultPage(WebDriver driver){
        super(driver);
    }

    public void movieFilterClick(){
        clickTo(movieFilter);
    }

    public MoviePage openMoviePage(String movie){
        List<WebElement> searchResultList = findElements(searchResultTable);
        for(WebElement searchResult : searchResultList) {
            if (searchResult.getText().equals(movie));
            searchResult.click();
            return new MoviePage(webDriver);
        }
        return null;
    }

    public List<String> getSearchResult() {
        List<String> movieNameList = new ArrayList<>();
        List<WebElement> searchResultList = findElements(searchResultTable);
        for(WebElement searchResult : searchResultList) {
            movieNameList.add(searchResult.getText());
        }
        return movieNameList;
    }
}
