package tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.Assert;

import page.MoviePage;
import page.SearchResultPage;
import utils.TestUtil;

public class MovieYearTest1 extends TestUtil {

    @DataProvider(name = "testData")
    public Object[][] getTestData(){
            return new Object[][]{
                    {"The Matrix", "31 March 1999 (USA)"},
                    {"The Matrix Reloaded", "15 May 2003 (USA)"},
                    {"The Matrix Revolutions", "5 November 2003 (USA)"},
                    {"The Godfather", "24 March 1972 (USA)"},
                    {"The Godfather: Part II", "18 December 1974 (USA)"},
                    {"The Godfather: Part III", "25 December 1990 (USA)"},
                    {"Inception", "16 July 2010 (USA)"},
                    {"The Shawshank Redemption", "14 October 1994 (USA)"},
                    {"12 Angry Men", "10 April 1957 (USA)"},
                    {"Fight Club", "15 October 1999 (USA)"}
            };
    }

    @Test(dataProvider = "testData")
    public void checkMovieYear(String movie, String expected) {
        System.out.println("Test started for " + movie + " thread id: " +  Thread.currentThread().getId());

        SearchResultPage searchResultPage = homePage.searchKeyword(movie);
        searchResultPage.movieFilterClick();
        MoviePage moviePage = searchResultPage.openMoviePage(movie);

        Assert.assertEquals(moviePage.getReleaseDate(), expected, "Release date error!");
    }
}
