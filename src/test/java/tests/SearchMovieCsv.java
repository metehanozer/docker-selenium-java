package tests;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.Assert;
import page.MoviePage;
import page.SearchResultPage;
import utils.TestUtil;

public class SearchMovieCsv extends TestUtil {

    @DataProvider(name = "movieList")
    public Iterator<Object []> getMovieList() {

        String csvFilePath = "movieList.csv";
        List<Object []> returnData = new ArrayList<>();
        String[] stringList= null;
        String line = null;

        try (
            FileReader fileReader = new FileReader(ClassLoader.getSystemResource(csvFilePath).getFile());
            BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            while ((line = bufferedReader.readLine()) != null) {
                stringList = line.split(",");
                returnData.add(stringList);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return returnData.iterator();
    }

    @Test(dataProvider = "movieList")
    public void searchMovieWithCsvData(String movie, String expected) {
        System.out.println("Test started for " + movie + " thread id: " +  Thread.currentThread().getId());

        SearchResultPage searchResultPage = homePage.searchKeyword(movie);
        searchResultPage.movieFilterClick();
        MoviePage moviePage = searchResultPage.openMoviePage(movie);

        Assert.assertEquals(moviePage.getReleaseDate(), expected, "Release date error!");
    }
}
