package tests;

import java.io.FileReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.Assert;
import page.SearchResultPage;
import utils.TestUtil;

public class SearchMovieJson extends TestUtil {

    @DataProvider(name = "movieList")
    public Object[][] getMovieList() {

        String jsonFilePath = "movieList.json";
        Object[][] returnData = null;

        try (FileReader reader = new FileReader(ClassLoader.getSystemResource(jsonFilePath).getFile())) {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            JSONArray jsonArray = (JSONArray) jsonObject.get("movieList");

            returnData = new Object[jsonArray.size()][2];
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jObject = (JSONObject) jsonArray.get(i);
                returnData[i][0] = jObject.get("movie");
                returnData[i][1] = jObject.get("expectedResult");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return returnData;
    }

    @Test(dataProvider = "movieList")
    public void searchMovieWithJsonData(String movie, String expected) {
        System.out.println("Test started for " + movie + " thread id: " +  Thread.currentThread().getId());

        SearchResultPage searchResultPage = homePage.searchKeyword(movie);
        searchResultPage.movieFilterClick();
        page.MoviePage moviePage = searchResultPage.openMoviePage(movie);

        Assert.assertEquals(moviePage.getReleaseDate(), expected, "Release date error!");
    }
}
