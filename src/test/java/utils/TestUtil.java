package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;
import java.net.MalformedURLException;
import java.net.URL;

import page.HomePage;

public class TestUtil {

    public WebDriver webDriver;
    protected HomePage homePage;

    @BeforeClass
    @Parameters({"browser"})
    public void setupDriver(@Optional("local-chrome") String browser) throws MalformedURLException {

        if (browser.equalsIgnoreCase("docker-chrome")) {
            webDriver = new RemoteWebDriver(new URL("http:localhost:4444/wd/hub"), DesiredCapabilities.chrome());
            webDriver.manage().window().maximize();

        } else if (browser.equalsIgnoreCase("docker-firefox")) {
            webDriver = new RemoteWebDriver(new URL("http:localhost:4444/wd/hub"), DesiredCapabilities.firefox());
            webDriver.manage().window().maximize();

        } else if (browser.equalsIgnoreCase("local-chrome")) {
            System.setProperty("webdriver.chrome.driver", System.getenv("CHROME_DRIVER"));
            webDriver = new ChromeDriver();
            webDriver.manage().window().maximize();

        } else if (browser.equalsIgnoreCase("local-firefox")) {
            System.setProperty("webdriver.gecko.driver", System.getenv("GECKO_DRIVER"));
            webDriver = new FirefoxDriver();
            webDriver.manage().window().maximize();

        } else {
            System.out.println("Vatdıfakisdet!");
            System.exit(1);
        }
    }

    @BeforeMethod(alwaysRun = true)
    public void homePage(){
        webDriver.get("https://www.imdb.com/");
        homePage = new HomePage(webDriver);
    }

    @AfterClass
    public void quitDriver() {
        webDriver.quit();
    }

}
